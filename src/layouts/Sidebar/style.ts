import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

interface ISidebar {
  isExpanded: boolean;
}

export const sidebarWrapper = styled.div<ISidebar>`
  width: ${({ isExpanded }) => (isExpanded ? '270px' : '70px')};
  min-height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  transition: 500ms ease-in-out width;
  background: #31353d;
  padding: 5% 10px;
  position: relative;
`;

export const sidebarChangeSize = styled.div`
  position: absolute;
  top: 10px;
  right: -40px;
  transform: translateX(-50%);
`;

export const sidebarHeader = styled.div`
  width: 100%;
  display: flex;
  margin-bottom: 25px;
  overflow: hidden;
`;

export const sidebarLogo = styled.img`
  width: 50px;
  flex-shrink: 0;
  height: 50px;
  border-radius: 5px;
`;

export const sidebarUserInfo = styled.div`
  margin-left: 20px;
  flex-grow: 1;
  display:flex;
  flex-direction:column;
`;

export const sidebarUserName = styled.div`
  span {
    color: #b8bfce;
    font-size: 21px;
  }
  span:last-child {
    font-weight: bold;
    margin-left: 10px
  }
`;
export const sidebarUserRole = styled.div`
  color: #818896;
  font-size: 14px
`;
export const sidebarLinkContainer = styled.div`
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const LinkText = styled.div<{ isExpanded: boolean }>`
  margin-left: 10px;
  overflow: hidden;
`;

export const Link = styled(NavLink).attrs({
  activeClassName: 'active',
})`
  &.active {
    background: #3a3f47;
  }
  &:hover {
    background: #3a3f47;
    transition: 0.2s ease-in-out background;
  }
  transition: 0.2s ease-in-out background;
  width: 100%;
  display: flex;
  align-items: center;
  color: #fff;
  text-decoration: none;
  padding: 5px 10px;
  border-radius: 4px;
  margin-bottom: 5px;
  &:last-child{
    margin-bottom: 0;
  }
`;
