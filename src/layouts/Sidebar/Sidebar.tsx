import React, { useState } from 'react';
import * as Styled from './style';
import { Button } from '../../components/Button/Button';
import { ReactComponent as Dots } from '../../assets/images/dots.svg';
import { ReactComponent as List } from '../../assets/images/list.svg';
import routes from '../../routes';
import { v4 as uuid } from 'uuid';

export const Sidebar = () => {
  const [isExpanded, setIsExpanded] = useState<boolean>(true);
  const handleExpanded = () => setIsExpanded(!isExpanded);
  return (
    <Styled.sidebarWrapper isExpanded={isExpanded}>
      <Styled.sidebarChangeSize>
        <Button onClick={handleExpanded} small round>
          {isExpanded ? <Dots /> : <List />}
        </Button>
      </Styled.sidebarChangeSize>
      <Styled.sidebarHeader>
        <Styled.sidebarLogo src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" />
        <Styled.sidebarUserInfo>
          <Styled.sidebarUserName>
            <span>Jhon</span>
            <span>Smith</span>
          </Styled.sidebarUserName>
          <Styled.sidebarUserRole>user</Styled.sidebarUserRole>
        </Styled.sidebarUserInfo>
      </Styled.sidebarHeader>
      <Styled.sidebarLinkContainer>
        {routes.map(route => (
          <Styled.Link to={route.path} key={uuid()}>
            <route.icon
              style={{
                flexShrink: 0,
              }}
            />
            <Styled.LinkText isExpanded={isExpanded}>
              {route.title}
            </Styled.LinkText>
          </Styled.Link>
        ))}
      </Styled.sidebarLinkContainer>
    </Styled.sidebarWrapper>
  );
};
