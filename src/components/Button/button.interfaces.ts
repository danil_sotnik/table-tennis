export interface IButton {
  small?: boolean;
  large?: boolean;
  round?: boolean;
  children?: any;
  background?: any;
  onClick: () => void;
}
