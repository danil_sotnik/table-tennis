import React, { FC } from 'react';
import { ButtonElement } from './style';
import { IButton } from './button.interfaces';

export const Button: FC<IButton> = (props) => {
  return <ButtonElement {...props} >{props.children}</ButtonElement>;
};
