import styled from 'styled-components';
import { IButton } from './button.interfaces';

const buttonParamsSmall = `
    min-width: 40px;
    height: 40px;
`;

const buttonParamsLarge = `
    min-width: 60px;
    height: 60px;
`;

export const ButtonElement = styled.button<IButton>`
  &:focus, &:active{
    border:none;
    outline: none;
  }
  border:none;
  outline: none;
  display:flex;
  align-items: center;
  justify-content:center;
  cursor: pointer;
  box-shadow: 0 2px 2px 0 rgba(153, 153, 153, 0.14), 0 3px 1px -2px rgba(153, 153, 153, 0.2), 0 1px 5px 0 rgba(153, 153, 153, 0.12);
  ${({ small }) => (small ? buttonParamsSmall : null)};
  ${({ large }) => (large ? buttonParamsLarge : null)};
  ${({ round }) => (round ? 'border-radius: 50%' : null)};
  background : ${({ background }) => (background ?  + background : '#ffffff')};
`;
