import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { Sidebar } from './layouts/Sidebar/Sidebar';
import * as Styled from './style';
import './style.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Styled.commonWrapper>
          <Sidebar />
          <Styled.contentPage>
            <Switch>
              <Route exact={true} path="/home" component={() => <div>a</div>} />
              <Route component={() => <div>not found</div>}/>
            </Switch>
            <Redirect from="/" to="/home" />
          </Styled.contentPage>
        </Styled.commonWrapper>
      </BrowserRouter>
    </div>
  );
}

export default App;
