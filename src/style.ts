import styled from 'styled-components';

export const contentPage = styled.div`
  width: 100%;
  min-height: 100vh;
`;

export const commonWrapper = styled.div`
  display: flex;
`;
