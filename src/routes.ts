import { ReactComponent as People} from './assets/images/group.svg';
import { ReactComponent as Home} from './assets/images/home.svg';
import { ReactComponent as Chart} from './assets/images/chart.svg';

export default [
  {
    title: 'Home',
    path: '/home',
    icon: Home,
  },
  {
    title: 'Members',
    path: '/members',
    icon: People,
  },
  {
    title: 'Statistic',
    path: '/statistic',
    icon: Chart,
  },
];
